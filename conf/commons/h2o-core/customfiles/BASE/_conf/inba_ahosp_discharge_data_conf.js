/*******************************************************************
 *
 * FILE DI CONFIGURAZIONE SCHEDA DIMISSIONE *
 * VERSIONE BASE 28.05.2014
 *
 ********************************************************************/
 var checkSDO2017 = false;
/*
 * Controlli che vengono fatti al salvataggio
 */
function configOnSave()
{
	AINLib.log('Controlli nel file conf...');
	
	/**************************************************************************
	* CAMPI OBBLIGATORI
	**************************************************************************/
	var checkList={
		'HOEDUCATION' : 'Titolo di studio',
		'HOOCCUPATION' : 'Professione',
		'HOCIVILSTATE' : 'Stato civile',
		'LAST' : 'Default'
	}
	
	/**************************************************************************
	* CONTROLLI PERSONALIZZATI
	**************************************************************************/	
	
	// 1. data intervento uguale o superiore a quella del ricovero ************
	var checkSurgeriesDateHosp = true;
	var checkSurgeriesDateHospMsg1 = 'La data della procedura \n\n#\n\n deve essere superiore a quella di ricovero';
	var checkSurgeriesDateHospMsgN = 'Le date delle procedure \n\n#\n\n devono essere superiori a quella di ricovero';
	
	// 2. data intervento minore od uguale a data dimissione (solo per dimissioni non protette)
	var checkSurgeriesDateDisch = true;
	var checkSurgeriesDateDischMsg1 = 'La data della procedura \n\n#\n\n deve essere inferiore a quella della dimissione';
	var checkSurgeriesDateDischMsgN = 'Le date delle procedure \n\n#\n\n devono essere inferiori a quella della dimissione';
	
	// 3. date procedure inferiori a data dimissione (solo per dimissioni non protette)
	var checkLastSurgery = true;
	var checkLastSurgeryMsg = 'La data di dimissione inserita � precedente alla data di conclusione di trasferimenti o procedure';
	
	// 4. data di prenotazione inferiore alla data ricovero********************
	var checkReservationHospDate = true;
	var checkReservationHospDateMsg = 'La data di prenotazione deve essere inferiore alla data di ricovero';
	
	// 5. data prenotazione obbligatoria in mancanza del presidio di provenienza
	var checkReservationDate = true;
	var checkResevationDateMsg = 'La data di prenotazione � obbligatoria';
	
	// 6. congruenza codice fiscale/cittadinanza
	var checkFiscalCode = true;
	var checkFiscalCode = 'Codice fiscale e cittadinanza non congruenti';
	
	// 7. istituto provenienza obbligatorio per tipo provenienza >=4 e <=8
	var checkOriginIRC = true;
	var checkOriginIRCMsg = 'Istituto di provenienza obbligatorio';
	
	// 8. trauma/intossicazione obbligatorio per diagnosi comprese fra 800 e 905 e fra 910 e 995
	var checkTrauma = true;
	var checkTraumaMsg = 'Trauma/intossicazione obbligatoria';
	
	// 9. istituto di trasferimento obbligatorio per trasferimento ad altro istituto
	// Nota Bene = se si attiva � necessario capire per quale tipo di trasferimento deve essere necessario.
	var checkTransferInstitute = true;
	var checkTransferInstituteMsg = 'Istituto di trasferimento obbligatorio';
	
	// 10. causa traumatismi esterna
	var checkExternalTrauma = true;
	var checkExternalTraumaMsg = 'Causa esterna di traumatismi # ';
	
	// 11. riscontro autoptico obbligatorio per i decessi
	var checkPostMortemExams = true;
	var checkPostMortemExamsMsg = 'Riscontro autoptico obbligatorio';
	
	// 12. data di dimissione non superiore al giorno corrente
	var checkDimissionDate = true;
	var checkDimissionDateMsg = 'La data di dimissione non pu� essere precedente o successiva al giorno corrente';
	
	// 13. data dimissione successiva alla data di ricovero
	var checkDischDate = true;
	var checkDischDateMsg = 'La data di dimissione deve essere successiva alla data di ricovero';
	
	// 14. medico dimettente obbligatorio se presente la data di dimissione
	var checkDischMedic = true;
	var checkDischMedicMsg = 'Medico dimettente obbligatorio';
	
	/**************************************************************************
	* IMPLEMENTAZIONE CONTROLLI
	**************************************************************************/

	var checkOnSaveError = '';
	var diagsChild = window.frames['inba_dhosp_diagnosis'];
	var surgsChild = window.frames['inba_dhosp_surgeries'];
	var msgs = [];
	
	// 1. data intervento uguale o superiore a quella del ricovero
	if (checkSurgeriesDateHosp)
	{
		if (!Empty(w_HODISCHDATETIME))
		{
			try
			{
				if (typeof surgsChild != 'undefined')
				{
					var surgeries = [];
			
					for (var i = 0; i < surgsChild.Rows(); i++)
					{
						if (i == surgsChild.m_nCurrentRow)
						{
							if ((GLib.DateTimeToDate(surgsChild.w_EVEVENTDATEBEGIN)) < (GLib.DateTimeToDate(w_HOHOSPDATETIME)))
							{
								surgeries.push(surgsChild.w_SURGERYDESCRIPTION);
							}
						}
						else
						{
							if ((GLib.DateTimeToDate(surgsChild.m_oTrs[i]['EVEVENTDATEBEGIN'])) < (GLib.DateTimeToDate(w_HOHOSPDATETIME)))
							{
								surgeries.push(surgsChild.m_oTrs[i]['SURGERYDESCRIPTION']);
							}
						}
					}
			
					if (surgeries.length == 1)
					{
						msgs.push(checkSurgeriesDateHospMsg1.replace(/#/g, surgeries));
					}
					else if (surgeries.length > 1)
					{
						msgs.push(checkSurgeriesDateHospMsgN.replace(/#/g, surgeries.join('\n')));
					}
				}	
			}
			catch(err) {}
		}	
	}
	
	// 2. data intervento minore od uguale a data dimissione (solo per dimissioni non protette)
	if (checkSurgeriesDateDisch)
	{
		// Condizione valida per il LAZIO
		//if (w_HODISCHTYPE != '5' && !Empty(w_HODISCHDATETIME))
		// Condizione valida per TOSCANA-EMILIA ROMAGNA
		if (w_HODISCHTYPE != '3' && w_HODISCHTYPE != '9' && !Empty(w_HODISCHDATETIME))
		{
			try
			{
				if (typeof surgsChild != 'undefined')
				{
					var surgeries = [];
			
					for (var i = 0; i < surgsChild.Rows(); i++)
					{
						if (i == surgsChild.m_nCurrentRow)
						{
							if ((GLib.DateTimeToDate(surgsChild.w_EVEVENTDATEBEGIN)) > (GLib.DateTimeToDate(w_HODISCHDATETIME)))
							{
								surgeries.push(surgsChild.w_SURGERYDESCRIPTION);
							}
						}
						else
						{
							if ((GLib.DateTimeToDate(surgsChild.m_oTrs[i]['EVEVENTDATEBEGIN'])) > (GLib.DateTimeToDate(w_HODISCHDATETIME)))
							{
								surgeries.push(surgsChild.m_oTrs[i]['SURGERYDESCRIPTION']);
							}
						}
					}
			
					if (surgeries.length == 1)
					{
						msgs.push(checkSurgeriesDateDischMsg1.replace(/#/g, surgeries));
					}
					else if (surgeries.length > 1)
					{
						msgs.push(checkSurgeriesDateDischMsgN.replace(/#/g, surgeries.join('\n')));
					}
				}
			}
			catch(err) {}
		}
	}
	
	// 3. date procedure inferiori a data dimissione (solo per dimissioni non protette)
	if (checkLastSurgery)
	{
		if (!Empty(w_HODISCHDATETIME))
		{
			// Condizione valida per il LAZIO
			//if (w_HODISCHTYPE != '5')
			// Condizione valida per TOSCANA-EMILIA ROMAGNA
			if (w_HODISCHTYPE != '3' && w_HODISCHTYPE != '9')
			{
				if (!(w_HODISCHDATETIME >= w_HOSPDATETIME  && w_HODISCHDATETIME >= w_HOSPLASTBEGIN && w_HODISCHDATETIME >= w_LASTPROCEDURE))
				{
					msgs.push(checkLastSurgeryMsg);
				}
			}
		}
	}
	
	// 4. data di prenotazione inferiore alla data ricovero
	if (checkReservationHospDate)
	{
		if (!Empty(w_DDRESSURGERYPROGDT))
		{
			if (w_DDRESSURGERYPROGDT > (GLib.DateTimeToDate(w_HOHOSPDATETIME)))
			{
				msgs.push(checkReservationHospDateMsg);
			}
		}
	}
	
	// 5. data prenotazione obbligatoria in mancanza del presidio di provenienza
	if (checkReservationDate)
	{
		if (Empty(w_HOORIGINIRC) && Empty(w_DDRESSURGERYPROGDT))
		{
			msgs.push(checkResevationDateMsg);
		}		
	}
	
	// 6. congruenza codice fiscale/cittadinanza
	if (checkFiscalCode)
	{
		if (w_HOCITIZENSHIP == '100' && ((w_HOFISCODE.indexOf('STP120') > -1) || (w_HOFISCODE.indexOf('ENI120') > -1)))
		{
			msgs.push(checkFiscalCodeMsg);
		}
	}
	
	// 7. istituto provenienza obbligatorio per tipo provenienza >=4 e <=8
	if (checkOriginIRC)
	{
		if (Number(w_HOSENTBY) >= 4 || Number(w_HOSENTBY <= 8))
		{
			if (Empty(w_HOSENTBY))
			{
				msgs.push(checkOriginIRCMsg);
			}
		}
	}
	
	// 8. trauma/intossicazione obbligatorio per diagnosi comprese fra 800 e 905 e fra 910 e 995
	if (checkTrauma)
	{
		try
		{
			if (typeof diagsChild != 'undefined')
			{
				for (var i = 0; i < diagsChild.Rows(); i++)
				{
					var id = Number (diagsChild.m_oTrs[i]['EVDIAGNOSISID']);
				
					if (id > 1000)
					{
						id = Math.round(id / 10);
					}
				
					if ((id >= 800) && (id <= 905) || (id >= 910) && (id <= 995))
					{
						if (Empty(w_HOTRAUMA))
						{
							msgs.push(checkTraumaMsg);
						}
					}
				}
			}
		}
		catch(err) {}
	}
	
	// 9. istituto di trasferimento obbligatorio per trasferimento ad altro istituto
	if (checkTransferInstitute)
	{
		// Condizione valida per il LAZIO
		//if (w_HODISCHTYPE == '2' && !Empty(w_HODISCHDATETIME))
		// Condizione valida per TOSCANA-EMILIA ROMAGNA
		if ((w_HODISCHTYPE == '6' || w_HODISCHTYPE == '8') && !Empty(w_HODISCHDATETIME))
		{
			if (Empty(w_HOTRANSFERINST))
			{
				msgs.push(checkTransferInstituteMsg);
			}
		}
	}
	
	// 10. causa traumatismi esterna
	if (checkExternalTrauma)
	{
		if (Empty(w_HOTRAUMA) || (w_HOTRAUMA == '0'))
		{
			if (!Empty(w_HOCAUSEEXTTRAUMA))
			{
				msgs.push(checkExternalTraumaMsg.replace(/#/g, 'non valida'));
			}
		}
		else
		{
			if (Empty(w_HOCAUSEEXTTRAUMA))
			{
				msgs.push(checkExternalTraumaMsg.replace(/#/g, 'obbligatoria'));
			}
		}
	}
	
	// 11. riscontro autoptico obbligatorio per i decessi
	if (checkPostMortemExams)
	{
		// Condizione valida per il LAZIO
		//if (w_HODISCHTYPE == '4' && !Empty(w_HODISCHDATETIME))
		// Condizione valida per TOSCANA-EMILIA ROMAGNA
		if (w_HODISCHTYPE == '1' && !Empty(w_HODISCHDATETIME))
		{
			if (Empty(w_HOPMEXAM) || w_HOPMEXAM == '0')
			{
				msgs.push(checkPostMortemExamsMsg);
			}
		}
	}
	
	// 12. data di dimissione non superiore al giorno corrente
	if (checkDimissionDate)
	{
	var dStart = new Date();
	dStart.setHours(00);
	dStart.setMinutes(00);
	dStart.setMilliseconds(000);
	dStart.setSeconds(00)
	var dEnd = new Date();
	dEnd.setHours(23);
	dEnd.setMinutes(59);
	dEnd.setMilliseconds(999);
	dEnd.setSeconds(59)
	if( !Empty(w_HODISCHDATETIME) && w_HODISCHDATETIME<dStart || w_HODISCHDATETIME>dEnd )
	{
	msgs.push(checkDimissionDateMsg);
	}
	}
	
	// 13. data dimissione successiva alla data di ricovero
	if (checkDischDate)
	{
		if (!Empty(w_HODISCHDATETIME) && (w_HODISCHDATETIME < w_HOHOSPDATETIME))
		{
			msgs.push(checkDischDateMsg);
		}
	}
	
	// 14. medico dimettente obbligatorio se presente la data di dimissione
	if (checkDischMedic)
	{
		if (!Empty(w_HODISCHDATETIME) && Empty(w_HODISCHARGEMEDICID))
		{
			msgs.push(checkDischMedicMsg);
		}
	}
	/**************************************************************************
	* GESTIONE CAMPI OBBLIGATORI
	**************************************************************************/
	
	jQuery("INPUT.Temp").removeClass('Temp Obligatory');

	var v;
	var e = '';

	for (var i in checkList)
	{
		if (i == 'LAST')
		{
			continue;
		}

		eval('v=w_' + i);
		
		if (Empty(v))
		{
			e += '\n  - ' + checkList[i];
			jQuery(Ctrl(i)).addClass("Temp Obligatory");
		}
	}
	
	/**************************************************************************
	* COMPOSIZIONE MESSAGGIO
	**************************************************************************/
	if (!Empty(e))
	{
		checkOnSaveError = 'I campi:' + e + '\nsono obbligatori';
	}
	else if (msgs.length > 0)
	{
		for (var i = 0; i < msgs.length; i++)
		{
			checkOnSaveError += msgs[i] + '\n';
		}
	}

	return checkOnSaveError;
}

function initParameters()
{
	if (Empty(w_HODISCHDATETIME))
	{
		Set_HODISCHDATETIME(DateTime());
	}

	if (Empty(w_HODISCHARGEMEDICID))
	{
		Set_HODISCHARGEMEDICID(w_g_Medic);
	}

	if (Empty(w_HODISCHTYPE))
	{
		Set_HODISCHTYPE(w_DEFAULTVALUE);
	}

	if (Empty(w_HOPMEXAM))
	{
		var hopm = loadDefaultHOPM();
		Set_HOPMEXAM(hopm);
	}

	if (Empty(w_HOFIXEDRATE))
	{
		Set_HOFIXEDRATE('1');
	}

	if (Empty(w_HOPACKET))
	{
		Set_HOPACKET('00');
	}

	if (Empty(w_HOEVENT))
	{
		Set_HOEVENT('0000000000');
	}

	if (!Empty(w_HOHOSPDISCHOUID))
	{
		var afo = loadDefaultAFO();
		Set_HOAFO(afo);
	}

	//BRUNI - popolamento automatico del campo 'Diagnosi di dimissione' con le note del ricovero.
	if (!Empty(w_HODIAGNOSISMEMO) && Empty(w_HODISCHARGEDIAGNOSIS))
	{
		Set_HODISCHARGEDIAGNOSIS(w_HODIAGNOSISMEMO);
	}
}
