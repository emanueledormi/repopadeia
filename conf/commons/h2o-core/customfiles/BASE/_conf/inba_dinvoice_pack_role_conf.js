var settingDefaults=false;

function setDefaults()
{
	var parentType = parent.w_IPTYPE;
  var parentAction = parent.EntityStatus();
  
  
  
  if(!settingDefaults && parentType=='F' && parentAction=='N' && Rows()==0){
    settingDefaults=true;
    
    //FARMACI SOMMINISTRAT
    GridAddEmptyRow();
    Set_RPCODFAM('45');
    
    //PARAFARMACI
    GridAddEmptyRow();
    Set_RPCODFAM("72");
    
    //DEGENZA (MC)
    GridAddEmptyRow();
    Set_RPARTCAT('279');    
    
    //SALA OPERATORIA (MC)
    GridAddEmptyRow();
    Set_RPARTCAT('148');
    
    //MATERIALI DI CONSUMO STERILIZZAZIONE
    GridAddEmptyRow();
    Set_RPCODART("A455");

    //MATERIALI DI CONSUMO DEGENZA
    GridAddEmptyRow();
    Set_RPCODART("A130");

    //MATERIALI DI CONSUMO SALA OPERATORIA
    GridAddEmptyRow();
    Set_RPCODART("A131");
    
    settingDefaults=false;
  }
}
