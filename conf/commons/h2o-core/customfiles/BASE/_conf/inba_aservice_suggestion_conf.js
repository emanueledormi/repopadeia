/*
* FUNZIONE DI CONFIGURAZIONE SALVATAGGIO 
*
* La funzione deve effettuare i controlli anche per i figli (se devono essere fatti).
* Deve ritornare stringa vuota se il salvataggio pu� procedere, la stringa d'errore altrimenti
*/
function configOnSave()
{
	/*
	*  CAMPI OBBLIGATORI PROPOSTA DI ATTIVITA'
	*/
	var checkList;

	if (w_FCWAITLIST != 'INP' && w_FCSERVICETYPE != '00004')
	{
		checkList = 
		{
			'FCLASTNAME' : 'Cognome',
			'FCFIRSTNAME' : 'Nome',
			'FCSEX' : 'Sesso',
			'FCBIRTHDATE' : 'Data di nascita',
			'FCPHONE1' : 'Cellulare',
			'FCWAITLIST' : 'Unit� di programmazione',
			'FCSERVICETYPE' : 'Tipologia proposta',
			'FCDATETIME' : 'Data proposta',
			'FCREQSTATE' : 'Stato della proposta',
			'FCMEDICID' : 'Medico specialista',
//			'FCREGIMEID (?) w_ISCLINIC != \'S\'' : 'Regime',
//			'FCDIAGNOSISDESCRI' : 'Descrizione diagnosi',
//			'FCOUID' : 'Unit� operativa',
//			'FCURGENCY(?)w_ISCLINIC != \'S\'' : 'Priorit�',
//			'FCHOSPITALIZATIONLENGTH (?) w_FCREGIMEID == \'OM\'' : 'Durata (gg)',
			'FCSURGERYLENGTH' : 'Durata',
//			'FCRESIDENCEADDRESS' : 'Indirizzo di Residenza',
			'LAST' : 'Default'
		};
	}
	else
	{
		checkList = 
		{
			'FCLASTNAME' : 'Cognome',
			'FCFIRSTNAME' : 'Nome',
			'FCSEX' : 'Sesso',
			'FCBIRTHDATE' : 'Data di nascita',
			'FCPHONE1' : 'Cellulare',
			'FCWAITLIST' : 'Unit� di programmazione',
			'FCSERVICETYPE' : 'Tipologia proposta',
			'FCDATETIME' : 'Data proposta',
			'FCREQSTATE' : 'Stato della proposta',
			'FCMEDICID' : 'Medico specialista',
			'LAST' : 'Default'
		};
	};
	
	
	var checkListPortlet = [];

	if (!Empty(w_g_Medic))
	{
		checkListPortlet['incc_wr012_history'] =
		{
			'cb_grado' : 'Grado',
			't_altezza' : 'Altezza',
			't_peso' : 'Peso',
			'r_malessere' : 'Domanda 1',
			'r_malattie' : 'Domanda 2',
			'r_respiro' : 'Domanda 3',
			'r_tosse' : 'Domanda 4',
			'r_dispnea' : 'Domanda 5',
			'r_angina' : 'Domanda 6',
			'r_farmaci' : 'Domanda 7',
			'r_allergia' : 'Domanda 8',
			'r_anestesia' : 'Domanda 9',
			'r_problemi_anestesia' : 'Domanda 10',
			'r_gravidanza' : 'Domanda 11',
			'r_malattia_sistemica' : 'Domanda 12',
			'LAST' : 'Default'
		}
	}


	/*
	* 1. CONTROLLO OBBLIGATORIETA' DELL'INTERVENTO CHIRURGICO 
	*/
	var checkSurgeryArchive = true;
	var checkSurgeryArchiveMsg = 'L\'intervento chirurgico � obbligatorio';

	/*
	 * 2. Controllo della SESSIONE
	 */
	var checkSession = true;
	var checkSessionMessage = 'Non � stato possibile determinare la sessione operatoria. Questo potrebbe portare problemi nel riordinamento degli interventi.';

	/*
	 * 3. CONTROLLO DIFFERENZA FRA DATA INTERVENTO E DATA PRELIEVO
	 */
	var checkBloodSample = false;
	var checkBloodSampleMsg = 'Data intervento non valida';

	var checkDiagnosi = true;
	var checkDiagnosiMsg = 'La diagnosi � obbligatoria';

	var checkDurata = true;
	var checkDurataMsg = 'La durata(gg) � obbligatoria';

	var checkOnere = false;
	var checkOnereMsg = 'L\'onere � obbligatorio';

	/********************************************************************************
	*  REGOLE DI VALIDAZIONE - FINE
	**********************************************************************************/

	/********************************************************************************
	*  Non modificare sotto!
	**********************************************************************************/
	var checkOnSaveError = '';
	var v, o, e1 = '';var nt;

	//*********************
	// obbligatoriet�
	//*********************
	for (var i in checkList)
	{
		if (i == 'LAST')
		{
			continue;
		}

		cond = "true";
		msg = checkList[i];

		if (i.indexOf("(?)") > 0)
		{
			var a = i.split("(?)");
			i = a[0];
			cond = a[1];
		}

		try
		{
			eval("ef=" + cond);
		}
		catch (e)
		{
			//Se qualcosa va storto mi metto nella condizione pi� stringente
			ef = true;
		}

		eval("v=w_" + i);

		if (ef && Empty(v))
		{
			e1 += "\n  - " + msg;
			jQuery(Ctrl(i)).addClass("Temp Obligatory");
		}
	}
	
	var e1add = [];
	
	if (Empty(e1)) //Faccio questo controllo solo se vanno bene tutti i campi della maschera
	{
		var score = 0;
		var noteValue = '';

		for (portlet in checkListPortlet)
		{
			if (checkListPortlet.hasOwnProperty(portlet))
			{
				portletPointer = ZtVWeb.getPortlet(portlet);

				if (Empty(portletPointer))
				{
					continue;
				}
				
				for (var i in checkListPortlet[portlet])
				{
					if (i == 'LAST')
					{
						continue;
					}
					
					cond = "true";
					msg = checkListPortlet[portlet][i];
					
					if (i.indexOf("(?)") > 0)
					{
						var a = i.split("(?)");
						i = a[0];
						cond = a[1];
					}
					
					try
					{
						eval("ef=" + cond);
					}
					catch (e)
					{
						ef = true;
					}
					
					eval('v=portletPointer.'+i+'.Value()');
					//metto l'if altrimenti lo fa anche per i campi che non sono del tipo t_xxxx (e quindi non hanno campo note)
					if (i.substr(0,2)=='r_') eval('nt=portletPointer.'+(i.replace('r','t'))+'.Value()');
					if(ef && Empty(v))
					{
						e1+="\n  Dati clinici: "+msg;
					}
					if (v=='SI' && Empty(nt)) score++;
				}  
			}
		}	
		if (score >0 )
		{
			e1add.push("Specificare i motivi del SI nei dati clinici");
		}
	}
	
	//Controllo valorizzazione intervento chirurgico. (quando la proposta di attivit� prevede un intervento chirurgico)
    if(checkSurgeryArchive)
	{
		//Il controllo � solo sul titolo...
		if(w_ISSURGERY == 'S' && Empty(w_MYSURGERY))
		{
			e1add.push(checkSurgeryArchiveMsg);
		}
    }
	
	if (checkDiagnosi)
	{
		if (Empty(w_FCDIAGNOSISDESCRI))
		{
			e1add.push(checkDiagnosiMsg);
		}
	}

	if (checkDurata)
	{
		if (Empty(w_FCHOSPITALIZATIONLENGTH) && (w_FCSERVICETYPE == '00002' || w_FCSERVICETYPE == 'FC'))
		{
			e1add.push(checkDurataMsg);
		}
	}

	if (checkOnere)
	{
		if (Empty(w_FCHONERE) && (w_FCSERVICETYPE == '00002' || w_FCSERVICETYPE == 'FC'))
		{
			e1add.push(checkOnereMsg);
		}
	}
	/*
	* 2. CONTROLLO DIFFERENZA FRA DATA INTTERVENTO E DATA PRELIEVO
	*/
	if (checkBloodSample == true)
	{
		if (!Empty(w_FCBLOODSAMPLEDATE) && !Empty(w_SURGERYTRESHOLD))
		{
			if (DateDiff(w_FCBLOODSAMPLEDATE, w_FCSURPLANNEDDATETIME) < w_SURGERYTRESHOLD)
			{
				e1add.push(checkBloodSampleMsg);
			}
		}
	}

	//Tenere questo per ultimo!!! Controllo 2
	if (checkSession && e1add.length == 0 && Empty(e1) && Empty(w_FCSESSIONTIME) && !Empty(w_FCSURPLANNEDDATETIME))
	{
		alert(checkSessionMessage);
	}
	
	//Composizione messaggio
	if (!Empty(e1))
	{
		checkOnSaveError = 'I campi: ' + e1 + '\nsono obbligatori'
	}
	else if (e1add.length > 0)
	{
		for (var i = 0; i < e1add.length; i++)
		{
			checkOnSaveError += e1add[i] + '\n';
		}
	}
  
	/*
	 * WARNING CHE NON PREVEDONO BLOCCO DEL SALVATAGGIO (se non c'� errore)
	 */
	var toWarn = false;
	
	if (Empty(checkOnSaveError))
	{
		if (toWarn)
		{
			checkOnSaveError = '#STOPWARNING#';
		}
	}

	return checkOnSaveError;
}


/*
* FUNZIONE DI CONFIGURAZIONE AL CARICAMENTO 
* Non � garantito l'avvenuto caricamento dei figli. Far riferimento alle rispettive funzioni 
*/
function configOnLoad()
{
	editLabel('Cognome', '', 'red');
	editLabel('Nome', '', 'red');
	editLabel('Sesso', '', 'red');
	editLabel('Data di nascita', '', 'red');
	editLabel('Cellulare', '', 'red');
	editLabel('Unit� di programmazione', '', 'red');
	editLabel('Tipologia proposta', '', 'red');
	editLabel('Data proposta', '', 'red');
	editLabel('Stato della proposta', '', 'red');
	editLabel('Medico specialista', '', 'red');
	editLabel('Regime', '', 'red');
	editLabel('Descrizione diagnosi', '', 'red');
	editLabel('Unit� operativa', '', 'red');
	editLabel('Priorit�', '', 'red');
	editLabel('Durata (gg)', '', 'red');
	editLabel('Durata', 'Durata (min)', 'red');
	editLabel('Indirizzo di Residenza', '', 'red');

	editLabel('Data ingresso prevista', '', 'green');
	editLabel('Note mediche', '', 'green');
	editLabel('Data intervento richiesta', '', 'green');
	editLabel('Indirizzo di residenza', '', 'green');
	editLabel('Civico', '', 'green');
	editLabel('Comune di domicilio', '', 'green');
	editLabel('Stato civile', '', 'green');
	editLabel('N� tessera sanitaria regionale', '', 'green');
	editLabel('Cittadinanza', '', 'green');
	editLabel('Codice attestato', '', 'green');
	editLabel('Sigla paese', '', 'green');
	editLabel('Identificativo personale', '', 'green');
	editLabel('Data fine validit� attestato', '', 'green');
	editLabel('Indirizzo email', '', 'green');
	editLabel('Titolo di studio', '', 'green');
	editLabel('Tipo documento', '', 'green');
	editLabel('Numero documento', '', 'green');
	editLabel('Data rilascio', '', 'green');
	editLabel('Consenso', '', 'green');
	editLabel('Dal', '', 'green');
	editLabel('Al', '', 'green');
	editLabel('Note', '', 'green');
	
	var jollyfields = {};

	/********************************************************************************
	*  DEFINIZIONE CAMPI JOLLY - INIZIO
	*
	*  Di seguito vengono esposti gli step di configurazione
	*  dei campi Jolly. I campi nella maschera devono essere gi� 
	*  configurati per funzionare correttamente, essere visibili 
	*  ed editabili (eventualmente sotto condizione) ed avere 
	*  associata la classe: jollyfield.
	*
	*  Il posizionamento e la dimensione all'interno della maschera 
	*  non hanno importanza dato che viene effettuato nuovamente qui.
	*
	*  Per CIASCUN CAMPO che si vuole aggiungere si deve ripetere il pezzo di codice che segue 
	*  configurandolo opportunamente 
	*  NB: Se il campo prevede lo zoom occorre indicare il nome fieldbox descrittivo
	**********************************************************************************/
	jollyfields["FCCONVOTYPE"] = 
	{
		"left": 0,
		"top": 0,
		"width": 0,
		"label": "Genere convenzione",
		"tooltip": "Genere della convenzione",
		"tabindex": 0,
		"edit":true
	};

	/********************************************************************************
	*  DEFINIZIONE CAMPI JOLLY - FINE
	**********************************************************************************/  

	/********************************************************************************
	*  Non modificare sotto!
	**********************************************************************************/

	AINLib.initJollyFields(jollyfields);
	editLabel('Genere convenzione', '', 'red'); //spostiamolo sotto! prova ora
}