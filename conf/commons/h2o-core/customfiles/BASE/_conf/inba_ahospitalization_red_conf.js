/***********************************************************
*
* FILE DI CONFIGURAZIONE MASCHERA RICOVERO                 * 
*
***********************************************************/


/*
* FUNZIONE DI CONFIGURAZIONE SALVATAGGIO 
*
* La funzione deve effettuare i controlli anche per i figli (se devono essere fatti).
* Deve ritornare stringa vuota se il salvataggio pu� procedere, la stringa d'errore altrimenti
*/

// NOTA BONINI - ALLA DATA ATTUALE (31.3.2015) NON VIENE NEMMENO CHIAMATA IN MATER DEI

function configOnSave() {

  /*
  * 1. CAMPI OBBLIGATORI 
  */
  var checkList1={
    //"_nome_fieldbox_":"_descrizione_", 
    
    
    
    "LAST":"Default" //Default, non rimuovere/commentare e deve essere SEMPRE ALL'ULTIMO POSTO
  }
  
  /********************************************************************************
  *  REGOLE DI VALIDAZIONE - INIZIO
  *
  *  Ciascuna check list corrisponde ad un'area dati della maschera. Commentare / rimuovere 
  * il commento per modificare le condizioni al salvataggio.  
  * NB: E' preferibile che il campo di controllo sia vibile all'utente (tra codice e descrizione
  * preferire la descrizione) in modo di dare visibilit� all'utente del campo da riempire
  **********************************************************************************/
  
  
  /*
  * Controllo data impegnativa
  */
  var dataImpegnativaCheck=false;
  var dataImpegnativaMesiValidita=6;
  var dataImpegnativaCheckMsg="L'impegnativa � scaduta";

  /*
  * 1. CONTROLLO CONFERIMENTO DELLA PRIVACY
  */
  var warnPrivacy=false;
  var warnPrivacyMsg="Attenzione!\nConsenso al trattamento dati personali scaduto o non ancora richiesto.\n\nIl paziente acconsente al trattamento dei dati personali?"
  //Di seguito si attiva la funzionalit� per cui se la privacy era scaduta viene chiesto all'utente se stampare il modulo della privacy
  var printPrivacyFlag=false; //Flag per la stampa 
  var printPrivacyMsg="Stampa consenso privacy per prestazioni ambulatoriali al salvataggio"
  var printPrivacyCode="0000000670";//Codice stampa da lanciare.  Il parametro passato al report � p_hospId oppure HOHOSPID
  var printPrivacyType="R";//Indica se si tratta di un Report oppure di un multireport (R\M)

  /********************************************************************************
  *  REGOLE DI VALIDAZIONE - FINE
  **********************************************************************************/  
  
  
  
  
  
  
  /********************************************************************************
  *  Non modificare sotto!
  **********************************************************************************/
  var checkOnSaveError="";
  
  
  //Rimuovo classi obbligatorio precedentemente inserite
  jQuery("INPUT.Temp").removeClass("Temp Obligatory");
  
  
  var v,o,e1="",e2="",e3="";
  var e1add=new Array();
  //Testata obbligatoriet�
  //*********************
  for(var i in checkList1) {
    if(i=='LAST')
      continue;
    
    eval("v=w_"+i);
    if(Empty(v)){
      e1+="\n  - "+checkList1[i];
      jQuery(Ctrl(i)).addClass("Temp Obligatory");
    }
  }
  
  
  if (dataImpegnativaCheck && !Empty(w_HOBOOKINGDATE))
  {
	
	var expirationDate = new Date(w_HOBOOKINGDATE);
	AddDays(expirationDate,1);
	expirationDate.setMonth(expirationDate.getMonth()+dataImpegnativaMesiValidita);
  
	if (expirationDate<( Empty(w_HOHOSPDATETIME)?w_HORESERVATIONDATETIME:w_HOHOSPDATETIME ))
	{
		e1add.push(dataImpegnativaCheckMsg);
		//break;
	}
  }
  
 
  //Altri controlli che richiedono query (le faccio una solo volta se riesco)
  //************************************************************************************
  if(Empty(e1) && Empty(e2) && Empty(e3) && e1add.length==0 ){
  }
   
  

  //Composizione messaggio
  if(!Empty(e1)){
    checkOnSaveError="I campi anagrafici:"+e1+"\nSono obbligatori"
  }else if(e1add.length>0) {
    for(var i=0;i<e1add.length;i++)
      checkOnSaveError+=e1add[i]+"\n";
  } 
  
  /*
  * WARNING CHE NON PREVEDONO BLOCCO DEL SALVATAGGIO (se non c'� errore)
  */
  var toWarn=false;
  if(Empty(checkOnSaveError)){
    
    //PRIVACY
    if(warnPrivacy){
      toWarn=checkPrivacy(warnPrivacyMsg, printPrivacyFlag, printPrivacyMsg, printPrivacyCode, printPrivacyType);
    }
    
    
    
    //Se siamo in caso di warning blocco il salvataggio
    if(toWarn)
      checkOnSaveError='#STOPWARNING#';
  }


  
  
  //debugger
  return checkOnSaveError; 
}












/*
* FUNZIONE DI CONFIGURAZIONE AL CARICAMENTO 
* Non � garantito l'avvenuto caricamento dei figli. Far riferimento alle rispettive funzioni 
*/
function configOnLoad() {
	
    var jollyfields={};
      
      
  /********************************************************************************
  *  DEFINIZIONE CAMPI JOLLY - INIZIO
  *
  *  Di seguito vengono esposti gli step di configurazione
  *  dei campi Jolly. I campi nella maschera devono essere gi� 
  *  configurati per funzionare correttamente, essere visibili 
  *  ed editabili (eventualmente sotto condizione) ed avere 
  *  associata la classe: jollyfield.
  *
  *  Il posizionamento e la dimensione all'interno della maschera 
  *  non hanno importanza dato che viene effettuato nuovamente qui.
  *
  *  Per CIASCUN CAMPO che si vuole aggiungere si deve ripetere il pezzo di codice che segue 
  *  configurandolo opportunamente 
  *  NB: Se il campo prevede lo zoom occorre indicare il nome fieldbox descrittivo
  **********************************************************************************/
  /* CODICE ESEMPIO
  jollyfields["NOMEFIELD"]={
    //Indicare la distanza in pixel dal bordo sinistro (0 = default = quella in maschera)
    "left": 0,
    //Indicare la posizione in altezza (0 = default = quella in maschera)
    "top": 0,
    //Indicare la larghezza del campo (0 = default = quella in maschera) TRIC: 100px circa 14 caratteri; 7px circa un carattere  
    "width": 0,
    //Indicare la label da associare al campo ("" = default = nessunal label)
    "label": "",
    //Indicare il tooltip da associare al campo ("" = default = tooltip definito in maschera)
    "tooltip": "",
    //Indicare il tab sequence (0 = default = quella in maschera)
    "tabindex": 0,
    //Indicare la condizione di editabilit� del campo 
    //NB: la condizione viene sovrascritta dalle logiche implementate in maschera. Se in maschera
    //non � specificata alcuna condizione statica vale questa condizione, che per� va intesta come 
    //STATICA ossia non dipendente da altre variabili. Pu� tornare comodo ad esempio per il controllo
    //dei permessi Utilities.IsInGroup(1)
    "edit":true
  }
  */
 
  
  
  
  /********************************************************************************
  *  DEFINIZIONE CAMPI JOLLY - FINE
  **********************************************************************************/  
  
  /********************************************************************************
  *  Non modificare sotto!
  **********************************************************************************/
    
  AINLib.initJollyFields(jollyfields);

  
}

function myOnLoadEnd ()
{
	
	if (EntityStatus()=='E' && !(Empty(w_HOHOSPDATETIME)))
	{
		db = DaysBetween(w_HOHOSPDATETIME,new Date());
		if (db>=7 && db%7 == 0)
		{
			weeks = Math.floor(db/7);
			alert('Il paziente � ricoverato da '+db+' giorni ('+weeks+' settimane)');
		}
	}
	
	//Verranno presentati con il bordo rosso
	var campiObbligatori = [
	"HOFIRSTNAME",
	"HOLASTNAME",
	"HOFISCALCODE",
	//"PARESIDCITYDESCRI",
	"HOHOSPREGIME",
	//"HOONERE",
	//"HOCIVILSTATE",
	//"HOEDUCATION",
	//"HOEDUCATION",
	//"DESCONV",
	//"HOCURRENTMEDICDESCRI",
	//"HOSENTYBYMEDICDESCRI",
	//"HORULESID",
	"WARDDESCRI",
	//"OUDESCRI",
	//"HOBIRTHCITYDESCRI",
	//"HOOCCUPATION"
	];
	
	/*if (w_HOHOSPSYSTEM==1)*/ //campiObbligatori.push("HOHOSPSYSTEMTYPE");
	/*if (w_HOHOSPSYSTEM==2)*/ //campiObbligatori.push("HODHSYSTEMTYPE");
	
	//MODIFICA BONINI CIRCOSCRIZIONE OBBLIGATORIA PER ROMA E TORINO (11.06.2015)
	//if(!(Empty(w_HORESIDCITYCODE)) || w_HORESIDCITYCODE != '')  {
	//	if (w_HORESIDCITYCODE == '100058091' || w_HORESIDCITYCODE == '100001272')  
	//		campiObbligatori.push("PARESIDENCEDISTRICTDESCRI"); 
	//}
	
	//Verranno presentati con il bordo arancio
	var campiUtili = 
	[ "PAPHONE1"];
	
	
  jQuery.each(campiObbligatori, function( index, value ) {
     jQuery("[name="+value+"]").css( {'border-color':'#D24F00','border-width':'1px','border-style':'solid'});
	});
  jQuery.each(campiUtili, function( index, value ) {
     jQuery("[name="+value+"]").css( {'border-color':'orange','border-width':'1px','border-style':'solid'});
	});
}



/********************************************************************************
* VARIABILI DI APPOGGIO 
* Non modificare sotto!
**********************************************************************************/



