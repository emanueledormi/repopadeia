/***********************************************************
*
* FILE DI CONFIGURAZIONE MASCHERA ACCETTAZIONE * 
* TESTATA                                                                        *
*
***********************************************************/



/*
* FUNZIONE DI CONFIGURAZIONE SALVATAGGIO 
*
* La funzione deve effettuare i controlli anche per i figli (se devono essere fatti).
* Deve ritornare stringa vuota se il salvataggio pu� procedere, la stringa d'errore altrimenti
*/
function configOnSave() {

  console.log('ConfigOnSave della prenotazione');
  /********************************************************************************
  *  REGOLE DI VALIDAZIONE - INIZIO
  *
  *  Ciascuna check list corrisponde ad un'area dati della maschera. Commentare / rimuovere 
  * il commento per modificare le condizioni al salvataggio.  
  * NB: E' preferibile che il campo di controllo sia vibile all'utente (tra codice e descrizione
  * preferire la descrizione) in modo di dare visibilit� all'utente del campo da riempire
  **********************************************************************************/
  /*
  * 1. CAMPI OBBLIGATORI ANAGRAFICA/TESTATA
  */
  var checkList1={
  
    "LAST":"Default" //Default, non rimuovere/commentare e deve essere SEMPRE ALL'ULTIMO POSTO
  }
  
  

  /*
  * 2. CAMPI OBBLIGATORI PRENOTAZIONE
  */
  var checkList2={
      
    "LAST":"Default" //Default, non rimuovere/commentare e deve essere SEMPRE ALL'ULTIMO POSTO
  }
  
  
  
  

  /*
  * 3. CONTROLLO LUNGHEZZA IMPEGNATIVA
  */
  var checkPrescriptionCodeLength=false; //true/false
  var checkPrescriptionCodeLengthMsg="Il codice dell'impegnativa deve essere di 15 o 16 caratteri"; //Messaggio d'errore

  /*
  * 4. CONTROLLO CONFERIMENTO DELLA PRIVACY
  */
  var warnPrivacy=false;
  var warnPrivacyMsg="Attenzione!\nConsenso al trattamento dati personali scaduto o non ancora richiesto.\n\nIl paziente acconsente al trattamento dei dati personali?"
  //Di seguito si attiva la funzionalit� per cui se la privacy era scaduta viene chiesto all'utente se stampare il modulo della privacy
  var printPrivacyFlag=false; //Flag per la stampa 
  var printPrivacyMsg="Stampa consenso privacy per prestazioni ambulatoriali al salvataggio";
  var printPrivacyCode="0000000039";//Codice stampa da lanciare.  Il parametro passato al report � p_hospId oppure HOHOSPID
  
  /*
   * 5. CONTROLLO CONGRUITA' TIPO/CONVENZIONE [Rif.OI - 9641]
  */
   var checkCongruity=false;
   var checkCongruityMessage = 'Incongruenza tra tipo prenotazione e convenzione' 

  /*
   * 6. CONTROLLO SOVRAPPOSIZIONE APPUNTAMENTI PAZIENTE
   * _DESCRIAPPUNTAMENTO_ � unplaceholder che viene riempito a runtime con la descrizione dell'appuntamento del paziente
  */
   var warnResOverlap=false;
   var warnResOverlapMessage = "La prenotazione si sovrappone ad un altro appuntamento del paziente: \n[%_DESCRIAPPUNTAMENTO_%] \n\nProcedere comunque al salvataggio?";

  /*
   * 7. BLOCCO FUORI AGENDA
   * Controllo che vincola l'utente a inserire solo appuntamenti in agenda o al pi� in overbooking
  */
   var checkFARes=true;
   var checkFAResMessage = "Non � possibile inserire appuntamenti manuali fuori dai giorni dell'agenda o indicati come indisponibilit�";
   
  /*
   * 8. BLOCCO SUPERAMENTO OB/FA CONSENTITI
   * Controllo che vincola l'utente a rispettare il tetto massimo di FA/OB ammessi dalla regola di agenda
  */
   var checkFAOBMax=false;
   var checkFAOBMaxMessage = "Superato il numero massimo di fuori agenda/overbooking ammessi dall'agenda";
  
  /********************************************************************************
  *  REGOLE DI VALIDAZIONE - FINE
  **********************************************************************************/  
  
  
  
  
  
  
  /********************************************************************************
  *  Non modificare sotto!
  **********************************************************************************/
  var checkOnSaveError="";
  var wservices=window.frames["inam_dreservation_serv"];
  
  //Rimuovo classi obbligatorio precedentemente inserite
  jQuery("INPUT.Temp").removeClass("Temp Obligatory");
  wservices.jQuery("INPUT.Temp").removeClass("Temp Obligatory");
  
  var v,o,e1="",e2="";
  
  //Testata obbligatoriet�
  //*********************
  for(var i in checkList1) {
    if(i=='LAST')
      continue;
    
    eval("v=w_"+i);
    if(Empty(v)){
      e1+="\n  - "+checkList1[i];
      jQuery(Ctrl(i)).addClass("Temp Obligatory");
    }
  }
  
  //Prenotazioni obbligatoriet�
  //*************************
  for(var i in checkList2) {
    if(i=='LAST')
      continue;
    
    for(var j=0; j<wservices.Rows() || j==wservices.m_nCurrentRow; j++) {
      if( j==wservices.m_nCurrentRow){
        eval("v=wservices.w_"+i);        
        if(Empty(v)){
          e2+="\n  - "+checkList2[i];
          wservices.jQuery(wservices.Ctrl(i)).addClass("Temp Obligatory");
          break;
        }
      }else{
        eval("v=wservices.m_oTrs[j]['"+i+"']");
        if(!(typeof v =="undefined")  //NB:Questa condizione mi consente di gestire i campi di testata che non sono inseriti dentro il transitorio
            && Empty(v)){
          e2+="\n  - "+checkList2[i];
          break;
        }
      }
      
    }
  }
  
  
  
  //Altri controlli prenotazioni su transitorio
  //***************************************
  var e2add=new Array();
  var referrals = [];
  var refmaxexam = [];
 
  for(var j=0; j<wservices.Rows() || j==wservices.m_nCurrentRow; j++) {
    
    //Controllo lunghezza impegnativa
    if(checkPrescriptionCodeLength){
      var l;
      if( j==wservices.m_nCurrentRow){
        l=(wservices.w_PRPRESCRIPTIONCODE).length;
        if(l>0 && (l<15 || l>16)){
          e2add.push(checkPrescriptionCodeLengthMsg);
          break;
        }
      }else{
        l=(wservices.m_oTrs[j]['PRPRESCRIPTIONCODE']).length;
        if(l>0 && (l<15 || l>16)){
          e2add.push(checkPrescriptionCodeLengthMsg);
          break;
        }
      }
    }
	
  }
    
	// Controlli congruit�
	if (checkCongruity){
		convenzione = inam_dreservation_serv.w_PRCONV;
		    
   
	}
  
  
  //Controllo FA
  if(checkFARes){
   var outage; var date_manuale; var res_manuale; var serv_manuale;
    for( var i=0; i<wservices.Rows() || i==wservices.m_nCurrentRow; i++){
		if (i == wservices.m_nCurrentRow) {
			outage = wservices.w_PROVBOUTAGE; 
			date_manuale = wservices.w_PRRESDATETIME;
			serv_manuale=wservices.w_PRSERVICEID;
			res_manuale=wservices.w_PRRESOURCEID;
		}
	    else  {
			outage = wservices.m_oTrs[i]["PROVBOUTAGE"]; 
			date_manuale =wservices.m_oTrs[i]["PRRESDATETIME"];
			serv_manuale=wservices.m_oTrs[i]["PRSERVICEID"]; 
			res_manuale=wservices.m_oTrs[i]["PRRESOURCEID"]; 
		}
        if(outage=="F")
		{ 
			if (!inam_fcheck_reservationday(serv_manuale,res_manuale,date_manuale))
				{
			        e2add.push(checkFAResMessage);
					break;
				} else { 
					console.log('Agenda trovata nel giorno, senza indisponibilit�');
				}
		} 
		else {
			console.log("Non � un fuori agenda");
		}
  }}
  
  //Controllo max OB/FA
  if(checkFAOBMax){
    var vqr,q,nrec;
    
    //Ricerco prenotazioni OB/FA
    for(var i=0; i<wservices.m_oTrs.length; i++){
      if(wservices.m_oTrs[i]["PROVBOUTAGE"]=="F" || wservices.m_oTrs[i]["PROVBOUTAGE"]=="O"){
        
        //Se ho pi� appuntamenti in OB/FA faccio fatica a fare una sola query... tocca che la faccio per ciascun caso
        vqr = new VisualQueryReader('inam_qcheck_maxobfa', 'p_resourceid=' + wservices.m_oTrs[i]["PRRESOURCEID"] 
                +"&p_day="+Format(wservices.m_oTrs[i]["PRRESDATETIME"],"DD-MM-YYYY")
                +"&p_resid="+(EntityStatus()=='N' ? 'aabbccddrr' : w_RERESID)
              , 10);
        q = vqr.executeQuery();
        nrec = q.getRowCount();
      
        if(nrec==0) {
          //Fine della disponibilit�
          e2add.push(checkFAOBMaxMessage);
          break;
        }
      }
    }
  }
  

  //Composizione messaggio
  if(!Empty(e1)){
    checkOnSaveError="I campi:"+e1+"\nSono obbligatori"
  }else if(!Empty(e2)){
    checkOnSaveError="I campi della prenotazione:"+e2+"\nSono obbligatori"
  } else if(e2add.length>0) {
    for(var i=0;i<e2add.length;i++)
      checkOnSaveError+=e2add[i]+"\n";
  }
  
  
  /********************************************************************************
  * WARNING CHE NON PREVEDONO BLOCCO DEL SALVATAGGIO (se non c'� errore)
  ***********************************************************************************/
  var toWarn=false;
  if(Empty(checkOnSaveError)){
    
    //PRIVACY
    if(warnPrivacy){
      toWarn=checkPrivacy(warnPrivacyMsg, printPrivacyFlag, printPrivacyMsg, printPrivacyCode);
    }
    
    //SOVRAPPOSIZIONE IMPEGNI PAZIENTE
    if(!toWarn && warnResOverlap){
      toWarn=checkOverlap(warnResOverlapMessage);
    }
    
    
    
    //Se siamo in caso di warning blocco il salvataggio
    if(toWarn)
      checkOnSaveError='#STOPWARNING#';
  }


  
  
  //debugger
  return checkOnSaveError; 
}




/*
* FUNZIONE DI CONFIGURAZIONE AL CALCULATE 
* Non � garantito l'avvenuto caricamento dei figli. Far riferimento alle rispettive funzioni 
*/
function configOnCalculate(){
  //Modifica della tipologia: inizializzo la convenzione
  
  
}
//Definizione variabili di OLD da inizializzare al load con il valore di partenza
var co_RERESTYPE="";






/*
* FUNZIONE DI CONFIGURAZIONE AL CARICAMENTO 
* Non � garantito l'avvenuto caricamento dei figli. Far riferimento alle rispettive funzioni 
*/
function configOnLoad() {
    var jollyfields={};
      
    //Init OLD per il calculate
    co_RERESTYPE=w_RERESTYPE;
	
	// struttura di default
  if(EntityStatus()!="V"){
    var vqReader = new VisualQueryReader('inba_qlocation_data', '', 0);
    var q = vqReader.executeQuery();
    
    if (q.getRowCount() > 0)
    {
      var r = q.getNthRow(0);
      Set_RESTRUCTUREID(r.getField('LCDEFAULTSTRUCTURE'));
    }
  }      
      
      
  /********************************************************************************
  *  DEFINIZIONE CAMPI JOLLY - INIZIO
  *
  *  Di seguito vengono esposti gli step di configurazione
  *  dei campi Jolly. I campi nella maschera devono essere gi� 
  *  configurati per funzionare correttamente, essere visibili 
  *  ed editabili (eventualmente sotto condizione) ed avere 
  *  associata la classe: jollyfield.
  *
  *  Il posizionamento e la dimensione all'interno della maschera 
  *  non hanno importanza dato che viene effettuato nuovamente qui.
  *
  *  Per CIASCUN CAMPO che si vuole aggiungere si deve ripetere il pezzo di codice che segue 
  *  configurandolo opportunamente 
  *  NB: Se il campo prevede lo zoom occorre indicare il nome fieldbox descrittivo
  **********************************************************************************/
  /* CODICE ESEMPIO
  jollyfields["NOMEFIELD"]={
    //Indicare la distanza in pixel dal bordo sinistro (0 = default = quella in maschera)
    "left": 0,
    //Indicare la posizione in altezza (0 = default = quella in maschera)
    "top": 0,
    //Indicare la larghezza del campo (0 = default = quella in maschera) TRIC: 100px circa 14 caratteri; 7px circa un carattere  
    "width": 0,
    //Indicare la label da associare al campo ("" = default = nessunal label)
    "label": "",
    //Indicare il tooltip da associare al campo ("" = default = tooltip definito in maschera)
    "tooltip": "",
    //Indicare il tab sequence (0 = default = quella in maschera)
    "tabindex": 0,
    //Indicare la condizione di editabilit� del campo 
    //NB: la condizione viene sovrascritta dalle logiche implementate in maschera. Se in maschera
    //non � specificata alcuna condizione statica vale questa condizione, che per� va intesta come 
    //STATICA ossia non dipendente da altre variabili. Pu� tornare comodo ad esempio per il controllo
    //dei permessi Utilities.IsInGroup(1)
    "edit":true
  }
  */
  /*
  jollyfields["PARESIDENCEDISTRICTDESCRI"]={
    //Indicare la distanza in pixel dal bordo sinistro (0 = default = quella in maschera)
    "left": 0,
    //Indicare la posizione in altezza (0 = default = quella in maschera)
    "top": 0,
    //Indicare la larghezza del campo (0 = default = quella in maschera) TRIC: 100px circa 14 caratteri; 7px circa un carattere  
    "width": 0,
    //Indicare la label da associare al campo ("" = default = nessunal label)
    "label": "Circoscrizione",
    //Indicare il tooltip da associare al campo ("" = default = tooltip definito in maschera)
    "tooltip": "Circoscrizione",
    //Indicare il tab sequence (0 = default = quella in maschera)
    "tabindex": 0,
    //Indicare la condizione di editabilit� del campo 
    //NB: la condizione viene sovrascritta dalle logiche implementate in maschera. Se in maschera
    //non � specificata alcuna condizione statica vale questa condizione, che per� va intesta come 
    //STATICA ossia non dipendente da altre variabili. Pu� tornare comodo ad esempio per il controllo
    //dei permessi Utilities.IsInGroup(1)
    "edit":true
  }
  */

  
  /********************************************************************************
  *  DEFINIZIONE CAMPI JOLLY - FINE
  **********************************************************************************/  
  
  
  
  
  
  
  /********************************************************************************
  *  Non modificare sotto!
  **********************************************************************************/
    
  AINLib.initJollyFields(jollyfields);
  
}




/********************************************************************************
* VARIABILI DI APPOGGIO 
* Non modificare sotto!
**********************************************************************************/
var warnExamsDuplicateChoiceIsTrue=false;







