/***********************************************
 *  FILE DI CONFIGURAZIONE PER I PORTLET *
 * ***********************************************
 *
 * NOTE: Questo file va importato nel portlet che si desidera configurare.
 * Richiede la libreria AINLib che quindi deve essere importata nel portlet PRIMA di questo file.
 * E' possibile creare delle sezioni di action script personalizzato per ciascun portlet all'interno
 * del quale gestire l'inizializzazione delle variabili, eseguire o meno query etc.
 * Dove nell'action script indicavo la parola "this" qui va indicata la lettera t
 * L'idPortlet pu� essere recuperato con: t.formid
 */

var AINPortlet_conf = (typeof AINPortlet_conf == "undefined" ? {
    "portlets" : {

        // 1. CONFIGURAZIONE PER PORTLET VISTA PRENOTAZIONI
        //####################################
        "inba_wreservations": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */
            /* NB: Il classico this viene sostituito dalla variabile t */

            //ESEMPI:
            //Imposto parametri di ricerca
            // t.w_number.Value(123);
            // t.w_date.Value(new Date());

            t.c_processed.Value('');
            t.c_cancelled.Value('N');
            t.c_deleted.Value('N');

            //Nascondo una immagine
            // t.newPrint.Hide();

            //Eseguo la query
            // t.doList.Query();

            //Ridefinisco la funzione "Nuova ricerca"
            // t.l_reset_Click = function() { alert("Nuova ricerca") }

            //valorizzazione della Struttura associata alla location dell'utente loggato:
            //t.cb_structure.Value(t.w_defStructureId.Value());

            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // LISTA ATTESA AMBULATORIALE
        //################################
        "inba_wamb_wait_list": function (t) {

            var d = new Date();
            d.setDate(d.getDate());
            t.pAGEFROM.Value(d);

        },

        // MY HOME - ELENCO ACCETTAZIONI
        //################################
        "mcpe_wpatient_accepted": function (t) {

            var d = new Date();
            d.setDate(d.getDate() - 90);
            t.w_FROM1.Value(d);

        },

        // 2. CONFIGURAZIONE PER PORTLET VISTA ACCETTAZIONI
        //####################################
        "inam_whosp_list": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */


            //t.c_labcenter.Value('00007');
            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // 2. CONFIGURAZIONE PER PORTLET VISTA ACCETTAZIONI BOCA
        //####################################
        "iog_inam_wboca_list": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            var d = new Date();
            //d.setDate(d.getDate() - 1);
            t.t_hospitalizationDateFrom.Value(d);
            t.t_hospitalizationDateTo.Value(d);
            t.ckcuruser.Value(false);
            //t.c_labcenter.Value('00007');
            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // ELENCO ATTIVITA'
        //################################
        "inam_whosp_todo_list": function (t) {
            if (t.w_selectedTab.Value() == 3) {
                return;
            }

            t.tbs_main.Select(3);
            t.openTab(2);
            t.w_selectedTab.Value(3);
        },

        // ACCOGLIENZA CARDIOLOGIA
        //################################
        "inam_wreception_list_card": function (t) {

            t.w_branch.Value('0021');

        },

        // 3. CONFIGURAZIONE PER PORTLET WORKLIST
        //#############################
        "inba_wworklist": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            /*
            //Rimuovo la selezione delle prenotazioni
            t.w_source_all.Value("'T','A','H','R','S','L'");
            for(var i=0; i<t.cb_source.Ctrl.childNodes.length; i++){
            if("P"==t.cb_source.Ctrl.childNodes[i].value){
            t.cb_source.Ctrl.childNodes[i].remove();
            }
            }
             */

            //INIT ORIG
            var d = new Date();
            d.setDate(d.getDate());
            t.t_dateFrom.Value(d);
            t.t_dateTo.Value(d);

            t.cb_structure.Value(t.g_Structure.Value());

            if (!Empty(t.g_Medic.Value())) {
                /*
                t.t_assignee_Lostfocus = function () {
                t.executeLink('inba_qmedic_list_delegation_iog', t.t_assignee, t.wAssigneeId, 'i_assignee_Click');
                }
                t.i_assignee_Click = function () {
                t.wParam.Value(t.t_assignee.Value());
                t.wOtherParamName.Value("pAmbito");
                t.wOtherParamValue.Value("WOR");
                t.wQuery.Value('inba_qmedic_list_delegation_iog');
                t.wCallback.Value('opener.<%= idPortlet %>.setAssigneeId');
                t.inba_zoom.Link();
                }*/
                // t.chk_My.Value(true);
                //  t.w_my.Value('S');
                if (!t.loadSource())
                    t.cb_source.Value('T');
                t.cb_state.Value('0');
                //t.doWorklist.SetOrderBy(t.orderby)
                //t.doWorklist.Query();
            } else {
                t.t_assignee_Lostfocus = function () {
                    t.executeLink('inba_qmedic_list_delegation', t.t_assignee, t.wAssigneeId, 'i_assignee_Click');
                }
                //  t.chk_My.Value(false);
                t.w_my.Value('N');
                if (!t.loadSource())
                    t.cb_source.Value('T');
                t.cb_state.Value('0');
            }
            t.orderby = ' REQUESTDATETIME asc ';
            t.search();
            //t.cb_serviceunit.Value(t.w_medicUnitService.Value())

            /*
            Max 04/05/2022

            Aggiungo cambio colori per tipologia di esame in worklist (amb blu, deg rosso)
            e aaggiungo letto del degente accanto al nome paziente
             */





            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // CONFIGURAZIONE PER PORTLET WORKLIST FKT
        //#####################################
        "incc_wcasemedic_dashboard": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            var d = new Date();
            d.setDate(d.getDate());
            t.t_data_esecuzione_da.Value(d);
            t.t_data_esecuzione_a.Value(d);

            t.cb_source.Value('AA');

            //t.orderby = ' DATAORAESECUZIONE asc ';
            t.OrderBy.Value("DATAORAESECUZIONE desc");
            //alert('ordino');
            t.search();
            // code...
            /*
            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // CONFIGURAZIONE PER PORTLET ELABORAZIONE TRACCIATI
        //#####################################
        "inba_wtrace_process": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            t.chk_confirmed.Value('N');

            // code...

            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // 4. CONFIGURAZIONE PER VISTA RICOVERI DA PRENOTARE
        //#####################################
        "inba_wres_list": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            // code...

            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // 5. CONFIGURAZIONE PER VISTA RICOVERI PRENOTATI
        //###################################
        "inba_wres_full_list": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */
            /*
            var d=new Date();
            t.pHORESDATEFROM.Value(AddDays(d,1));
            t.pHORESDATETO.Value(AddDays(d,1));
            t.chk_onlyRes.Value(true);
             */
            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // 99. CONFIGURAZIONE PER VISTA SITUAZIONE REPARTO
        //###################################
        "inba_wrepa_letti_occ": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */


            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // 99. CONFIGURAZIONE PER PORTLET SOMMINISTRAZIONI
        //###################################
        "inba_whosp_administration": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            /*
            var d = new Date();
            d.setDate(d.getDate()-360);
            t.t_dataini.Value(d);
             */
            t.w_portletMode.Value('new'); // modalit� possibili: old/new
            t.manageActions();
            if (t.p_SOURCE.Value() == "X")
                t.t_dataini.Value(new Date());
            orderedQuery(t.administrationGrid, t.doHospAdministration, 'EVEVENTCREATIONDATE', false);

            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // 6. CONFIGURAZIONE PER VISTA RICOVERATI
        //#############################
        "inba_whosp_list": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            // credenziali wi-fi:inizio
            //sovrascrivo la funzione actionImage1
            /*
            t.actionImage1=function()
        {
            var p=document.getElementById("action1");
            p.parentNode.innerHTML='<span id="action1" class="layer_title">Genera password wi-fi: </span>';
            return getImage('silk/lock_open.png','genera password wi-fi','SpTheme_2');
            }
            //sovrascrivo la funzione action1. rs � il resultSet della riga.
            t.action1=function(rs)
        {
            t.sp_action1.Servlet('inba_fgenerate_wifi_pwd');
            t.sp_action1.Action('function');
            t.sp_action1.Parameters('p_HOHOSPCOUNTER='+rs.HOHOSPCOUNTER+',p_HOHOSPID='+rs.HOHOSPID+',p_HOHOSPYEAR='+rs.HOHOSPYEAR);
            if(confirm('Procedere con la generazione della password per la wi-fi?'))
        {
            var a=t.sp_action1.Link();
            if(a=='OK')
        {
            alert('Password generata con successo');
            }
            }
            else
        {
            return;
            }
            }
            // credenziali wi-fi: fine
             */
            t.OrderBy.Value("HOHOSPDATETIME desc");

            /*
            Autore: Max
            Data: 09/05/2022
            Richiedente: IOG - Nicoletti
            Richiesta: Se utente accede alla location Ufficio SCPS la portlet dei ricoverati fa vedere tutti i pazienti
             */
            // calcolo la location di accesso dell'utente
            var Location;
            var vqr = new VisualQueryReader('iog_qgetAccessLocation');
            var q = vqr.executeQuery();
            if (q.getRowCount() > 0) {
                var rs = q.getNthRow(0);
                var Location = rs.getField(0); //CODICE LOCATION
            }
            if (Location == '00016') {
                t.pPATIENTSTATE.Value('0')
            }

            /* FINE ACTION CODE PERSONALIZZATO */

        },

   // 13. LISTA DI ATTESA
  //################################
  "inba_wwait_lists" : function(t) { 
    /* INIZIO ACTION CODE PERSONALIZZATO */
    var preset1 = document.createElement("option") ;
	preset1.text = 'Da pianificare';
	preset1.value = 'preset1';
	var preset2 = document.createElement("option") ;
	preset2.text = 'Inseriti oggi';
	preset2.value = 'preset2';
	var preset3 = document.createElement("option") ;
	preset3.text = 'Attesi al ricovero';
	preset3.value = 'preset3';
	var preset5 = document.createElement("option") ;
	preset5.text = 'Attesi al preoperatorio';
	preset5.value = 'preset5';
  var preset6 = document.createElement("option") ;
	preset6.text = 'In evidenza';
	preset6.value = 'preset6';
	var preset7 = document.createElement("option") ;
	preset7.text ='Preop. da pian. con predeposito';
	preset7.value = 'preset7';
	var preset8 = document.createElement("option") ;
	preset8.text ='Da pianificare (ordinato)';
	preset8.value = 'preset8';
	t.cb_presets.Ctrl.add(preset1);
	t.cb_presets.Ctrl.add(preset2);
	t.cb_presets.Ctrl.add(preset3);
	t.cb_presets.Ctrl.add(preset5);
	t.cb_presets.Ctrl.add(preset6);
	t.cb_presets.Ctrl.add(preset8);
	//t.cb_presets.Ctrl.add(preset7);
	t.apply= function (value){
		t.cleanUp();
		switch(value) {
			case 'preset1':
			//	t.filters.cb_ricov.Value('N');
				t.filters.cb_insurg.Value('N');
			//	t.filters.cb_nopo.Value('N');
				t.filters.cb_sodi.Value('N');
				t.filters.cb_dimessi.Value('N');
				t.filters.cb_status_el.Value('1 - Richiesta medico');
				t.filters.cb_preplan.Value('N');
			//	t.filters.cb_status.Ctrl.options[1].selected = true;
				t.doElements.SetOrderBy("PATIENT desc");
				t.doElements.Query();
				
				break;
			case 'preset2':
				t.filters.t_listCreationFrom.Value(new Date());
				t.doElements.Query();
				break;
			case 'preset3':
				t.filters.t_surgeryGuessDay.Value(new Date());
				t.doElements.Query();
				break;
			case 'preset5':
				t.filters.t_prehosp.Value(new Date());
				t.doElements.Query();
				break;
				
			case 'preset6':
				t.filters.cb_evidenza.Value(1);
				t.doElements.Query();
				break;
			case 'preset7':
				t.filters.cb_sodi.Value('S');
			//	t.filters.cb_bloodsack.Value('N');
				t.doElements.Query();
				break;
			case 'preset8':
		//	t.filters.cb_ricov.Value('N');
			t.filters.cb_insurg.Value('N');
		//	t.filters.cb_nopo.Value('N');
			t.filters.cb_sodi.Value('N');
			t.filters.cb_dimessi.Value('N');
			t.filters.cb_preplan.Value('N');
		//	t.filters.cb_status.Ctrl.options[1].selected = true;
			t.doElements.SetOrderBy("FCDATETIME");
			t.doElements.Query();
			
			break;
		}
		
	}
	
	  //Variabile per visualizzare nella colonna precedente alla data intervento assegnata la data del preoperatorio anzich� la data intervento richiesta.
 //Data intervento richiesta --> w_isCustomColumn='N';
 //Data preoperatorio --> w_isCustomColumn='S';
    t.w_isCustomColumn.Value('S');
 if(t.w_isCustomColumn.Value()=='S')
 {
  t.replaceTitle('Data PO');
 }
    /* FINE ACTION CODE PERSONALIZZATO */
  },
  


        // 7. CONFIGURAZIONE PER VISTA DIMESSI
        //#############################
        "inba_whosp_discharged_list": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

      

        },

        //TAB DIMISSIONI SCHEDA PAZIENTE
        //#############################
        "inba_whosp_header": function (t) {

        },

        // 8. CONFIGURAZIONE PER VISTA TUTTI I RICOVERI
        //################################
        "inba_whosp_full_list": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            // code...

            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // 9. CONFIGURAZIONE PER FUNZIONIGRAMMA
        //################################
        "inba_wfunctionality": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            //Nascondo l'icona delle deleghe
            //t.l_delegation.Hide();
            //t.i_delegation.Hide();

            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // 10. VISITA PAZIENTE
        //################################
        "inam_wvisit_list_medic": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            //Convenzione PRIVATA
            //t.w_privato.Value("PRIV");

            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // 11. RICHIESTE ESAMI SENZA RISORSA
        //################################
        "inba_wexams_wresource": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */
            /*
            var d = new Date();
            d.setDate(d.getDate());
            t.t_preDateFrom.Value(d);
            t.t_preDateTo.Value(d);

            t.t_requestDateFrom.Value('');

            t.w_prenotati.Value('');
            t.cb_resource.Value('T');
            t.cb_state.Value('R');
            t.cApprovata.Value(true);
            t.w_approvata.Value('A');

            t.cb_examType.Value('L');

            }
             */
            /* FINE ACTION CODE PERSONALIZZATO */
        },

        // 11.B RICHIESTE ESAMI SENZA RISORSA - iog - Max
        //################################
        "iog_wexams_wresource": function (t) {
            /* INIZIO ACTION CODE PERSONALIZZATO */

            var d = new Date();
            d.setDate(d.getDate());
            //t.t_preDateFrom.Value('d');
            //t.t_preDateTo.Value('');
            //t.t_requestDateFrom.Value('d');
            //t.t_requestDateTo.Value('d');
            t.w_prenotati.Value('');
            t.cb_resource.Value('T');
            t.cb_state.Value('');
            t.cApprovata.Value(true);
            t.w_approvata.Value('A');
            t.cb_examOrigin.Value('R');
            t.cb_urgenza.Value('');
            //t.cb_examType.Value('L');

            /* Autore: Graziano
            Data: 05/10/2022 */
            t.CambioColore = function (s) {
                switch (s) {
                case 'S':
                    return "<span style='color:red;font-weight:bold' >" + s + "</span>";
                    break;
                case 'N':
                    return "<span style='color:blue;font-weight:bold' >" + s + "</span>";
                    break;
                }
            }
            t.gExams.Cols[8].enable_HTML = true;
            t.gExams.Cols[8].field = "function:CambioColore('%EVURGENCY%')";

            /*
            var rows = document.getElementsByClassName("grid_row");
            for (var i = 0; i < rows.length; i++) {
            //rows[i].getElementsByTagName("img")[0].style.visibility='visible';
            if (rows[i].cells.item(8).innerText == 'S')
            rows[i].style.backgroundColor = 'orange';
            }
            var rows = document.getElementsByClassName("grid_rowodd");
            for (var i = 0; i < rows.length; i++) {
            if (rows[i].cells.item(8).innerText == 'S')
            rows[i].style.backgroundColor = 'orange';
            }
             */

            t.MyFormatString = function (str) {
                var a = new String(arguments[0]);
                var b = new String(arguments[1]);
                var c = new String(arguments[2]);

                if (c == 'format1') {
                    a = a.substring(8, 10) + '-' + a.substring(5, 7) + '-' + a.substring(0, 4);
                }
                if (c == 'format2') {
                    if (a != '') {
                        a = a.substring(0, 6) + '20' + a.substring(6, 8)
                    };
                }
                if (c == 'format3') {
                    a = a.substring(0, 4) + '/' + a.substring(9, 16)
                }
                if (b == 'S') {
                    a = "<span style='color:red;font-weight:bold' >" + a + "</span>";
                }

                return a;
            }
            t.gExams.Cols[0].enable_HTML = true;
            t.gExams.Cols[0].field = "javascript:function:MyFormatString('%prestazione%','%evurgency%')";
            t.gExams.Cols[1].enable_HTML = true;
            t.gExams.Cols[1].field = "javascript:function:MyFormatString('%WARDDESCRIPTION%','%evurgency%')";
            t.gExams.Cols[2].enable_HTML = true;
            t.gExams.Cols[2].field = "javascript:function:MyFormatString('%PATIENTDESCRIPTION%','%evurgency%')";
            t.gExams.Cols[3].enable_HTML = true;
            t.gExams.Cols[3].field = "javascript:function:MyFormatString('%MEDICALHISTORY%','%evurgency%','format3')";
            t.gExams.Cols[4].enable_HTML = true;
            //t.gExams.Cols[4].field = "javascript:function:MyFormatString('%EVREQUESTDATEFORMAT%','%evurgency%','format2')";
            t.gExams.Cols[4].field = "javascript:function:MyFormatString('%EVREQUESTDATEFORMAT%','%evurgency%')"; // 11-05-2023 GRAZIANO - Modifiche su richiesta del cliente. Ticket #89129
            t.gExams.Cols[5].enable_HTML = true;
            t.gExams.Cols[5].title = "Data Esecuzione <br> Richiesta"
            //t.gExams.Cols[5].field = "javascript:function:MyFormatString('%EVPLANDATE%','%evurgency%','format1')";
            t.gExams.Cols[5].field = "javascript:function:MyFormatString('%EVPLANDATE%','%evurgency%')"; // 11-05-2023 GRAZIANO - Modifiche su richiesta del cliente. Ticket #89129
            t.gExams.Cols[6].enable_HTML = true;
            t.gExams.Cols[6].title = "Data Esecuzione <br> Effettuata"
            //t.gExams.Cols[6].field = "javascript:function:MyFormatString('%TPEXECUTIONDATE%','%evurgency%','format2')";
            t.gExams.Cols[6].field = "javascript:function:MyFormatString('%TPEXECUTIONDATE%','%evurgency%')"; // 11-05-2023 GRAZIANO - Modifiche su richiesta del cliente. Ticket #89129
            t.gExams.Cols[7].enable_HTML = true;
            t.gExams.Cols[7].title = "Data <br> Refertazione"
            //t.gExams.Cols[7].field = "javascript:function:MyFormatString('%TPREPORTDATE%','%evurgency%','format2')";
            t.gExams.Cols[7].field = "javascript:function:MyFormatString('%TPREPORTDATE%','%evurgency%')"; // 11-05-2023 GRAZIANO - Modifiche su richiesta del cliente. Ticket #89129
            /* Fine Graziano */
        }

        /* FINE ACTION CODE PERSONALIZZATO */
    },

    // 12. DA ACCETTARE
    //################################
    "inam_wmain": function (t) {
        /* INIZIO ACTION CODE PERSONALIZZATO */

        t.t_reservationDateTo.Value(new Date());

        t.cb_structures.Value(t.g_Structure.Value());

        t.w_patientOrderBy.Value('RESERVATIONDATETIME');
        t.doPrescriptions.SetOrderBy('RESERVATIONDATETIME');

        /* FINE ACTION CODE PERSONALIZZATO */
    },

    // LISTA REFERTI
    //################################
    "inba_wreports_list": function (t) {
        /* INIZIO ACTION CODE PERSONALIZZATO */

        t.p_docext.Value("'pdf'");
        t.doReports.Query();

        /* FINE ACTION CODE PERSONALIZZATO */
    },

    //IMPOSTAZIONI EDITOR
    //################################
    "inba_wckeditor": function (t) {
        CKEDITOR.instances.ckEditor.config.enterMode = CKEDITOR.ENTER_BR;
    },

    //TESTATA SCHEDA PAZIENTE LUNGODEGENZA
    //################################
    "inlc_whosp_header": function (t) {

        t.inlc_dinvoice_open.Parameters("CONVENTIONID=" + t.w_CONVENZIONE.Value() + ",HOSPID=" + t.pHOHOSPID.Value()
             + ",TIPOAC=" + t.w_TIPOACC.Value()
             + ",initfromparms=True"
             + ",CICLO=3P"
             + ",DATADAL=" + t.HOHOSPDATETIME.Value()
             + ",DATAAL=" + Format(new Date(), "DD-MM-YYYY")
             + ",DATADALA=" + t.HOHOSPDATETIME.Value()
             + ",DATAALA=" + Format(new Date(), "DD-MM-YYYY")
             + ",PAGATORE=P"
             + ",DAPAGA=" + t.w_customerid.Value()
             + ",APAGA=" + t.w_customerid.Value());
    },

    //SOMMINISTRAZIONI
    //################################
    "inba_whosp_administration": function (t) {
        t.w_portletMode.Value('old'); // modalit� possibili: old/new
        t.manageActions();
        if (t.p_SOURCE.Value() == "X")
            t.t_dataini.Value(new Date());
        orderedQuery(t.administrationGrid, t.doHospAdministration, 'EVEVENTCREATIONDATE', false);

        /* FINE ACTION CODE PERSONALIZZATO */
    },

    //RICETTA ELETTRONICA
    //################################
    "inam_wdigital_prescription": function (t) {
        t.w_restype.Value('00001');

        /* FINE ACTION CODE PERSONALIZZATO */
    },

    //...altre configurazioni


    /* ####################################### *
     *      NON MODIFICARE SOTTO!!!                                                    *
     * #######################################  */
    "_last_": function () { /*none*/
    }
}
    : AINPortlet_conf);

if (typeof AINLib == "undefined" || !AINLib) {
    alert("Missing AINLib library!!!");
} else {
    //Definisco la funzione al caricamento
    if (typeof AINPortlet_conf["load"] == "undefined") {
        AINPortlet_conf["load"] = function (e) {
            var pp;
            for (var p in AINPortlet_conf["portlets"]) {
                pp = ZtVWeb.getPortlet(p)
                    if (typeof pp != "undefined") {
                        //AINLib.log("Load configuration for ["+p+"]");
                        AINPortlet_conf["portlets"][p](pp);
                    }
            }
        }
    }
    AINLib.addEvent(window, "load", AINPortlet_conf.load);
}
